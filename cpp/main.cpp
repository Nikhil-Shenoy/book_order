#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <boost/tokenizer.hpp>
#include <typeinfo>

#define MAX_ORDER 1024
#define MAX_TITLE 256

using namespace std;

void read_book_order(string book_file) {

	ifstream books(book_file.c_str());

	char line[MAX_ORDER];

	string title;
	double price;
	int ID;
	string category;

	while(books.getline(line,MAX_ORDER)) {

		typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
		boost::char_separator<char> sep("|");
		string new_line(line);
		tokenizer tokens(new_line,sep);

		int count = 1;
		for(tokenizer::iterator it = tokens.begin(); it != tokens.end(); ++it) {

			switch(count) {
				case 1:
					title = *it;
					break;
				case 2:
					// price = stoi(str_price,nullptr);
					{
						string str_price = *it;
						price = stod(str_price,nullptr);
						break;
					}
				case 3:
					{
						string str_ID = *it;
						ID = stoi(str_ID,nullptr);
						break;
					}
				case 4:
					category = *it;
					break;
			}

			count++;
		}

		cout << title << " " << price << " " << ID << " " << category << endl << endl;

		// Create Book Order object and place it in the buffer.
	}


	books.close();

}


int main(int argc, char *argv[]) {

	if(argc != 4) {
		cerr << "Improper input. Usage: ./mt <database file> <book order file> <category file>" << endl;
		exit(1);
	}

	string database(argv[1]);
	string book_order(argv[2]);
	string category(argv[3]);

	read_book_order(book_order);

}