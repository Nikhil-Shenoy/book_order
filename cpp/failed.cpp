#include "../h/failed.h"

Failed::Failed(std::string new_title, double new_price)
	:title(new_title), price(new_price)
{}

void Failed::set_title(std::string new_title) {
	title = new_title;
}

std::string Failed::get_title() {
	return title;
}

void Failed::set_price(double new_price) {
	price = new_price;
}

double Failed::get_price() {
	return price;
}