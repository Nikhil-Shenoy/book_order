#include "../h/success.h"

Success::Success(std::string title, double price, double new_balance) 
	:Failed(title,price) {
		balance = new_balance;
	}

void Success::set_balance(double new_balance) {
	balance = new_balance;
}

double Success::get_balance() {
	return balance;
}