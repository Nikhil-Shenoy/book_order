#include "../h/customer.h"

Customer::Customer(std::string new_name, int new_id, double new_balance) 
	:name(new_name), ID(new_id), balance(new_balance)
{}

void Customer::set_name(std::string new_name) {
	name = new_name;
}

std::string Customer::get_name() {
	return name;
}

void Customer::set_ID(int new_ID) {
	ID = new_ID;
}

int Customer::get_ID() {
	return ID;
}

void Customer::set_balance(double new_balance) {
	balance = new_balance;
}

double Customer::get_balance() {
	return balance;
}

void Customer::increment_balance(double increase) {
	balance += increase;
}

void Customer::decrement_balance(double decrease) {
	balance -= decrease;
}