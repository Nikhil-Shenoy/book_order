#include "../h/order.h"

Order::Order(std::string new_title, double new_price, int new_ID, std::string new_category) 
	:title(new_title),
	 price(new_price),
	 ID(new_ID),
	 category(new_category)
{}

void Order::set_title(std::string new_title) {
	title = new_title;
}

std::string Order::get_title() {
	return title;
}

void Order::set_price(double new_price) {
	price = new_price;
}

double Order::get_price() {
	return price;
}

void Order::set_ID(int new_ID) {
	ID = new_ID;
}

int Order::get_ID() {
	return ID;
}

void Order::set_category(std::string new_category) {
	category = new_category;
}

std::string Order::get_category() {
	return category;
}