#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <vector>
#include <string>
#include "success.h"
#include "failed.h"

class Customer {

	public:
		Customer(std::string name, int id, double balance);

		void set_name(std::string new_name);
		std::string get_name();

		void set_ID(int new_ID);
		int get_ID();

		void set_balance(double new_balance);
		double get_balance();

		void increment_balance(double increase);
		void decrement_balance(double decrease);

		// void add_success(Success new_success);
		// void add_failure(Failed new_failure);

	private:
		std::string name;
		int ID;
		double balance;
		std::vector<Success> successes;
		std::vector<Failed> failures;
};

#endif