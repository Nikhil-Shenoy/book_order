#ifndef ORDER_H
#define ORDER_H

#include <string>

class Order {

	public:
		Order(std::string title, double price, int ID, std::string category);

		void set_title(std::string new_title);
		std::string get_title();

		void set_price(double new_price);
		double get_price();

		void set_ID(int new_ID);
		int get_ID();

		void set_category(std::string new_category);
		std::string get_category();

	private:
		std::string title;
		double price;
		int ID;
		std::string category;
};

#endif