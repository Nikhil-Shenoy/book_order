#ifndef SUCCESS_H
#define SUCCESS_H

#include "failed.h"

class Success : public Failed {

	public:
		Success(std::string title, double price, double balance);

		void set_balance(double new_balance);
		double get_balance();

	private:
		double balance;

};

#endif