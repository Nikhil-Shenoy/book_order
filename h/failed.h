#ifndef FAILED_H
#define FAILED_H

#include <string>

class Failed {
	
	public:
		Failed(std::string title, double price);

		void set_title(std::string new_title);
		std::string get_title();

		void set_price(double new_price);
		double get_price();

	private:
		std::string title;
		double price;

};

#endif