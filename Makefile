CC = g++
OBJS = main.o order.o failed.o success.o customer.o
SRC = cpp
H = h
CFLAGS = -Wall -g -std=c++11 -c -I /usr/local/lib/boost_1_64_0 -pthread
LFLAGS = -Wall -g -std=c++11 -g -pthread

mt: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o mt

main.o: $(SRC)/main.cpp
	$(CC) $(CFLAGS) -pthread $(SRC)/main.cpp

order.o: $(SRC)/order.cpp $(H)/order.h
	$(CC) $(CFLAGS) $(SRC)/order.cpp

failed.o: $(SRC)/failed.cpp $(H)/failed.h
	$(CC) $(CFLAGS) $(SRC)/failed.cpp	

success.o: $(SRC)/success.cpp $(H)/failed.h $(H)/success.h
	$(CC) $(CFLAGS) $(SRC)/success.cpp		

customer.o: $(SRC)/customer.cpp $(H)/failed.h $(H)/success.h
	$(CC) $(CFLAGS) $(SRC)/customer.cpp	

clean:
	rm -rf *.o mt
